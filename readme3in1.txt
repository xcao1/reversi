===========================================================================================
===========================================================================================
===========================================================================================

Reversi Protocol
Cheng Chen, Yizhou Lin

Sep 16 2012

This file is about the protocol of Reversi in the final project of CS541.
Rules of the game Reversi can be found in http://en.wikipedia.org/wiki/Reversi

The connection is based on TCP/IP. You can find the examples about how to establish the connection and how to send/receive data to/from the server in the client demos.

After the TCP/IP connection was established, the Client should send its name in the following format,

Nteamname

where teamname is a string no longer than 10 characters, and it should contain only alphabets, numbers and underscores(_).

When the server accepts 2 connections and their names, the game will start.
The server will randomly decide who plays first. The player who plays first is Black/Dark. The player who doesn't play first is White/Light.
The server will send the following message to the Black player:

U1

and it will send the following message to the White player:

U0

In each turn, the server will send the board situation to the player who is going to place a piece, in the following format,

Bn0n1n2n3n4...n63

which is an alphabet "B" followed by 64 numbers. The ith number ni indicates the square(xi,yi) in the board, where xi = int(i/8) and yi = i mod 8 (for example if i = 20, then xi = int(i/8) = 2 and yi = 20 mod 8 = 4). The left-lower corner is (0,0), the left-upper corner is (0,7), the right-lower corner is (7,0) and the right-upper corner is (7,7). Each number ni will be either 0, 1 or 2, which means the square(xi,yi) is either empty(0), occupied by White(1) or occupied by Black(2).

The client should place a piece in T seconds after the server sent the board situation, where T is the time limit for each move. The message of placing a piece at (x,y) is in the following format,

Mxy

If the client sends an illegal move, then the server will send the board situation to that client again; otherwise the server will update the board, and send the board situation to the next player.


When neither of the players can place legal moves, the game will be over. The server will decide the winner. The following message will be sent to the winner client,

W1

and the following message will be sent to the loser client,

W0

If it is a draw/tie then the server send the following message to both the clients,

W2

Finally the server will send a single character,

G

which indicates the game is over. Then the server will close all the connections, and wait for new connections to start a new game.


If you have any questions, please contact ylin8@stevens.edu.

===========================================================================================
===========================================================================================
===========================================================================================

ReversiClient v1.0
Yizhou Lin
ylin8 at Stevens.edu

Last Update: Sep 23 2012

0. Install Java

You should install JRE/JDK before compiling or running this program.
JRE or JDK can be downloaded from http://www.java.com.


1. Compile

Compiling is not necessary.

To compile this program,
run "javac ReversiClient.java".

You need JDK to compile it.


2. Run

To run this program,
run "java ReversiClient IP PORT",
where IP is the server's IP address and PORT is the port number.


3. Example

(type) java ReversiClient 127.0.0.1 4000
(type) Nteamname
(wait)
U1
B0000000000000000000000000002100000012000000000000000000000000000
(type) M42
(wait)
B0000000000000000000000000011100000222000000000000000000000000000
(type) M22
(wait)
...
...
...
...
(type) M00
W1
G


4. Other
This program was tested under WINDOWS 7 64-Bit, Java 1.7.0_07 64-Bit.
If you have any questions please contact ylin8@stevens.edu.


===========================================================================================
===========================================================================================
===========================================================================================


ReversiServer v1.0
Yizhou Lin
ylin8 at Stevens.edu

Last Update: Sep 23 2012


This program will be used as a server. Please run it before runing any clients.

It will print out everything it receives and sends, which will make you easier to debug your own client. It will also print the board situation in each turn.
Please notice that you does not have time limit for each move now, but you will have when you play against another client in the final project.


0. Install Java

You should install JRE/JDK before running this program.
JRE or JDK can be downloaded from http://www.java.com.


1. Run

To run this program,
run "java ReversiServer PORT",
where PORT is the port number.



2. Example

(type) java ReversiServer 4000
ReversiServer: Starting
ReversiServer: Waiting for connections
(wait)
ReversiServer: accpted a connection from /127.0.0.1
(wait)
ReversiServer: Receive <- 127.0.0.1 : Nteamname
(wait)
ReversiServer: accpted a connection from /127.0.0.1
(wait)
ReversiServer: Receive <- 127.0.0.1 : NClient2
ReversiServer: Send -> teamname : U1
ReversiServer: Send -> Client2 : U0
ReversiServer: BLACK : teamname
ReversiServer: WHITE : Client2
ReversiServer: Send -> teamname : B0000000000000000000000000002100000012000000000000000000000000000
(wait)
ReversiServer: Receive <- teamname : M42
........
........
........
...WB...
...BB...
....B...
........
........
ReversiServer: Send -> Client2 : B0000000000000000000000000002100000222000000000000000000000000000
(wait)
ReversiServer: Receive <- Client2 : M22
........
........
........
...WB...
...BB...
....B...
........
........
ReversiServer: Send -> Client2 : B0000000000000000000000000002100000222000000000000000000000000000

...
...
...


ReversiServer: Send -> teamname : W1
ReversiServer: Send -> teamname : G
ReversiServer: Send -> Client2 : W0
ReversiServer: Send -> Client2 : G
ReversiServer: Winner: BLACK (teamname)
ReversiServer: Restarting
ReversiServer: Waiting for connections
(wait)


3. Other
This program was tested under WINDOWS 7 64-Bit, Java 1.7.0_07 64-Bit.
If you have any questions please contact ylin8@stevens.edu.
