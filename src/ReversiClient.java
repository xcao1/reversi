/*
 * ReversiClient v1.0
 * Yizhou Lin
 * ylin8 at Stevens.edu
 * 
 * Last Update: Sep 23 2012
 * 
 * This is a very simple program that shows how the client connects to the server.
 * 
 * It mainly has two parts/threads.
 * SendThread is a thread that read everything from standard input and then write send them to the socket; and
 * ReceiveThread is a thread that read everything from the socket and then write them to the standard output.
 */

import java.io.*;
import java.net.Socket;
import java.util.Vector;

public class ReversiClient {
	public static String IP;
	public static int port;
	public static ReversiAI mAI;

	public static void main(String[] args) throws IOException {
		System.out.println("Please input IP : ( example : 127.0.0.1 )");
		BufferedReader input = new BufferedReader(new InputStreamReader(
				System.in));
		IP = input.readLine();

		System.out.println("Please input Port : ( example : 4000 )");
		input = new BufferedReader(new InputStreamReader(System.in));
		port = Integer.parseInt(input.readLine());

		Socket s;

		try {
			s = new Socket(IP, port);

			// socket input
			BufferedReader in = new BufferedReader(new InputStreamReader(
					s.getInputStream()));
			// socket output
			PrintWriter out = new PrintWriter(s.getOutputStream());


			ReversiAI panda = new ReversiAI(in, out);
			panda.go();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

// ==================================================
// data
class myInt {
	public int value;
	public myInt(int value) {
		this.value = value;
	}
}
class Pack {
	public boolean updated = false;
	public String message;
}
// ===================================================
// game
class Grid {
	public int x;
	public int y;
	public Grid(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
class Piece extends Grid {
	public int mColor;
	public Piece(int x, int y) {
		super(x, y);
		mColor = 0;
	}

	public Piece(int x, int y, int color) {
		super(x, y);
		mColor = color;
	}
}

class State {
	public int mCurrentPlayer;
	public int mBoard[][] = new int[8][8];

	public State(int player, int board[][]) {
		this.mCurrentPlayer = player;
		this.mBoard = board;
	}
}
// =================================================================================
// AI
class ReversiAI {
	public BufferedReader in;
	public PrintWriter out;

	public int turnCount = 0;
	public static boolean begin = true;
	public Pack mSend;
	public Pack mReceive;
	public int mColor;// 2 - black, 1 - white
	public int board[][] = new int[8][8];// [y][x], 0 - empty, 1 - white, 2 -
											// black

	public ReversiAI(BufferedReader br, PrintWriter o) {
		in = br;
		out = o;
		mReceive = new Pack();
		mSend = new Pack();
		this.turnCount = 0;
	}

	public void go() {
		while (true) {
			if (begin == true) {
				mSend.message = "Npanda";
				/*try {
					do {
						System.out
								.println("Please input team Name : ( example : Nname )");
						mSend.message = new BufferedReader(
								new InputStreamReader(System.in)).readLine();
					} while (mSend.message.charAt(0) != 'N');
				} catch (IOException e) {
					e.printStackTrace();
				}*/
				out.println(mSend.message);
				out.flush();
				begin = false;
			}
			if (mReceive.updated == false) {
				try {
					mReceive.message = in.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
				mReceive.updated = true;
			}
			if (mReceive.updated == true) {
				// print
				if (mReceive.message.length() <= 2) {// if not a board message
					if (mReceive.message.charAt(0) == 'U') {
						if (mReceive.message.charAt(1) - '0' == 1) {
							System.out.println("Black - X");
							mColor = 2;
						} else {
							System.out.println("White - O");
							mColor = 1;
						}
					} else if (mReceive.message.charAt(0) == 'W') {
						int val = mReceive.message.charAt(1) - '0';
						if (val == 1)
							System.out.println("Win");
						else if (val == 0)
							System.out.println("Lose");
						else if (val == 2)
							System.out.println("Draw");
					}
					System.out.println(mReceive.message);
				} else {
					updateBoard(mReceive.message);
					// print the board
					printMyBoard(mReceive.message);

					behave();
					
				}
				// reset
				mReceive.updated = false;
			}
		}

	}

	// =========================================
	public boolean directionLegal(final Piece move, int paramX, int paramY) {
		return directionLegal(move, paramX, paramY, null);
	}

	public boolean directionLegal(final Piece move, int paramX, int paramY,
			myInt endCount) {
		// test if this position is legal to make a move
		int x = move.x;
		int y = move.y;
		int color = move.mColor;
		int count = 0;
		for (int n = 1; y + n * paramY < 8 && y + n * paramY >= 0
				&& x + n * paramX < 8 && x + n * paramX >= 0; n++) {
			if (board[y + n * paramY][x + n * paramX] == 0) {
				return false;
			} else if (board[y + n * paramY][x + n * paramX] == color) {
				if (count == 0) {
					return false;
				} else {
					if (endCount != null)
						endCount.value = count;
					return true;
				}
			} else {
				count++;
				// continue;
			}
		}
		return false;
	}

	// ============================================
	public double scoringBoard(final State current) {
		double weight_1 = 120.00;
		double weight_2 = 100.00;
		double weight_3 = 90.00;
		double weight_4 = 2.0;
		double weight_5 = 6.0;
		double weight_6 = 0.2;
		double weight_trap_1 = 8;
		// double weight_trap_2 = 0.05;
		double weight_trap_3 = 0.6;

		double weight_lines_1 = 44;
		double weight_lines_2 = 240;
		// =================
		int sum = 0;
		// check lines
		boolean allMine = true;
		// horizontal
		for (int i = 2; i != 6; i++) {
			allMine = true;
			if (i != 3 && i != 4) {
				for (int j = 0; j != 8; j++) {
					if (current.mBoard[i][j] != this.mColor) {
						allMine = false;
					}
					if (allMine == true) {
						sum += weight_lines_1;
					}
				}
			}
		}
		for (int j = 2; j != 6; j++) {
			allMine = true;
			if (j != 3 && j != 4) {
				for (int i = 0; i != 8; i++) {
					if (current.mBoard[i][j] != this.mColor) {
						allMine = false;
					}
					if (allMine == true) {
						sum += weight_lines_1;
					}
				}
			}
		}
		// edges
		for (int i = 0; i != 8; i++) {
			allMine = true;
			if (i == 0 || i == 7) {
				for (int j = 0; j != 8; j++) {
					if (current.mBoard[i][j] != this.mColor) {
						allMine = false;
					}
					if (allMine == true) {
						sum += weight_lines_2;
					}
				}
			}
		}
		for (int j = 0; j != 8; j++) {
			allMine = true;
			if (j == 0 || j == 7) {
				for (int i = 0; i != 8; i++) {
					if (current.mBoard[i][j] != this.mColor) {
						allMine = false;
					}
					if (allMine == true) {
						sum += weight_lines_2;
					}
				}
			}
		}
		// ==================================
		// check single grid
		for (int i = 0; i != 8; i++) {
			for (int j = 0; j != 8; j++) {
				// traps
				if (((i == 2 || i == 5) && (j == 0 || j == 7))
						|| ((j == 2 || j == 5) && (i == 0 || i == 7))) {
					if (current.mBoard[i][j] == this.mColor) {
						sum += weight_trap_1;
					} else if (current.mBoard[i][j] == 0) {
					} else {
						sum -= weight_trap_1;
					}
				}
				if (((i == 1 || i == 6) && (j == 2 || j == 3 || j == 4 || j == 5))
						|| ((i == 0 || i == 7) && (j == 3 || j == 4))) {
					if (current.mBoard[i][j] == this.mColor) {
						sum -= weight_trap_1;
					} else if (current.mBoard[i][j] == 0) {
					} else {
						sum += weight_trap_1;
					}
				}
				if (((j == 1 || j == 6) && (i == 2 || i == 3 || i == 4 || i == 5))
						|| ((j == 0 || j == 7) && (i == 3 || i == 4))) {
					if (current.mBoard[i][j] == this.mColor) {
						sum -= (weight_trap_1 - 2);
					} else if (current.mBoard[i][j] == 0) {
					} else {
						sum += (weight_trap_1 - 2);
					}
				}
				if((i == 0 || i == 7 )&&(j==3 || j==4)){
					if (current.mBoard[i][j] == this.mColor) {
						sum -= (weight_trap_1 - 2);
					} else if (current.mBoard[i][j] == 0) {
					} else {
						sum += (weight_trap_1 - 2);
					}
				}
				/*
				 * if (((i == 2 || i == 5) && (j == 1 || j == 6)) || ((j == 2 ||
				 * j == 5) && (i == 6 || i == 1))) { if (current.mBoard[i][j] ==
				 * this.mColor) { sum += weight_trap_2; } else if
				 * (current.mBoard[i][j] == 0) { } else { sum -= weight_trap_2;
				 * } }
				 */

				if ((i == 2 || i == 5) && (j == 2 || j == 5)) {
					if (current.mBoard[i][j] == this.mColor) {
						sum += weight_trap_3;
					} else if (current.mBoard[i][j] == 0) {
					} else {
						sum -= weight_trap_3;
					}
				}
				// four angle
				if ((i == 0 && j == 0) || (i == 0 && j == 7)
						|| (i == 7 && j == 0) || (i == 7 && j == 7)) {
					if (current.mBoard[i][j] == this.mColor) {
						sum += weight_1;
					} else if (current.mBoard[i][j] == 0) {
					} else {
						sum -= weight_1;
					}
				} else if ((i == 1 && j == 1) || (i == 1 && j == 6)
						|| (i == 6 && j == 1) || (i == 6 && j == 6)) {
					if (current.mBoard[i][j] == this.mColor) {
						sum -= weight_2;
					} else if (current.mBoard[i][j] == 0) {
					} else {
						sum += weight_2;
					}
				} else if ((i == 0 && (j == 1 || j == 6))
						|| (i == 1 && (j == 0 || j == 7))
						|| (i == 6 && (j == 0 || j == 7))
						|| (i == 7 && (j == 1 || j == 6))) {
					if (current.mBoard[i][j] == this.mColor) {
						sum -= weight_3;
					} else if (current.mBoard[i][j] == 0) {
					} else {
						sum += weight_3;
					}
				}
				// edges
				else if (i == 0 || i == 7 || j == 0 || j == 7) {
					if (current.mBoard[i][j] == this.mColor) {
						sum += weight_4;
					} else if (current.mBoard[i][j] == 0) {
					} else {
						sum -= weight_4;
					}
				} else if (i == 1 || i == 6 || j == 1 || j == 6) {
					if (current.mBoard[i][j] == this.mColor) {
						sum -= weight_5;
					} else if (current.mBoard[i][j] == 0) {
					} else {
						sum += weight_5;
					}
				} else if (i == 2 || i == 5 || j == 2 || j == 5) {
					if (current.mBoard[i][j] == this.mColor) {
						sum += weight_6;
					} else if (current.mBoard[i][j] == 0) {
					} else {
						sum += weight_6;
					}
				}
			}
		}
		return sum;
	}

	// ==============================
	public double weightBoard(){
		double paramBoard_2 = 1;
		/*double weight = Math.log(this.turnCount + 1)
				+ paramBoard_2;*/
		double weight = this.turnCount/20+paramBoard_2;
		double result;
		
		result = weight;
		
		return result;
	}
	// =====================================
	public double weightMobility() {
		
		double paramMobility_2 = 120-Math.log(0.1);
		double weight = Math.log(this.turnCount/10 + 0.1)
				+ paramMobility_2;
		//double weight = this.turnCount/6+paramMobility_2;
		double result;
		
		result = weight;
		
		return result;
	}

	// ======================================================
	// scoring function
	public double Utility(int mobility, final State current) {
		double score_board = weightBoard()*scoringBoard(current);
		double score_mobility = weightMobility()*mobility;
		double result = score_board + score_mobility;

		return result;
	}

	// ==================================
	public boolean actionLegal(final State currentState, final Grid position) {
		// if this gird is available to make a move,
		// return a result
		// else, return null
		Piece move = new Piece(position.x, position.y,
				currentState.mCurrentPlayer);
		// test right
		if (directionLegal(move, 1, 0))
			return true;
		// test right down
		if (directionLegal(move, 1, -1))
			return true;
		// test down
		if (directionLegal(move, 0, -1))
			return true;
		// test left down
		if (directionLegal(move, -1, -1))
			return true;
		// test left
		if (directionLegal(move, -1, 0))
			return true;
		// test left up
		if (directionLegal(move, -1, 1))
			return true;
		// test up
		if (directionLegal(move, 0, 1))
			return true;
		// test right up
		if (directionLegal(move, 1, 1))
			return true;

		return false;
	}

	// ==================================
	// Result function
	public State Result(final State currentState, final Grid position) {
		// if this gird is available to make a move,
		// return a result
		// else, return null
		Piece move = new Piece(position.x, position.y,
				currentState.mCurrentPlayer);

		int directions[] = new int[8];
		for (int i = 0; i != 8; i++) {
			directions[i] = 0;
		}
		myInt count = new myInt(0);
		// test right
		if (directionLegal(move, 1, 0, count))
			directions[0] = count.value;
		// test right down
		if (directionLegal(move, 1, -1, count))
			directions[1] = count.value;
		// test down
		if (directionLegal(move, 0, -1, count))
			directions[2] = count.value;
		// test left down
		if (directionLegal(move, -1, -1, count))
			directions[3] = count.value;
		// test left
		if (directionLegal(move, -1, 0, count))
			directions[4] = count.value;
		// test left up
		if (directionLegal(move, -1, 1, count))
			directions[5] = count.value;
		// test up
		if (directionLegal(move, 0, 1, count))
			directions[6] = count.value;
		// test right up
		if (directionLegal(move, 1, 1, count))
			directions[7] = count.value;

		boolean flag = false;
		for (int i = 0; i != 8; i++) {
			if (directions[i] > 0) {
				flag = true;
			}
		}
		if (flag == false) {
			return null;
		} else {
			int newBoard[][] = new int[8][8];
			for (int i = 0; i != 8; i++) {
				for (int j = 0; j != 8; j++) {
					newBoard[i][j] = currentState.mBoard[i][j];
				}
			}
			newBoard[move.y][move.x] = currentState.mCurrentPlayer;
			for (int i = 0; i != 8; i++) {
				if (directions[i] > 0) {
					switch (i) {
					case 0:// right
						for (int j = 1; j != directions[i] + 1; j++) {
							newBoard[move.y][move.x + j] = currentState.mCurrentPlayer;
						}
						break;
					case 1:// right down
						for (int j = 1; j != directions[i] + 1; j++) {
							newBoard[move.y - j][move.x + j] = currentState.mCurrentPlayer;
						}
						break;
					case 2:// down
						for (int j = 1; j != directions[i] + 1; j++) {
							newBoard[move.y - j][move.x] = currentState.mCurrentPlayer;
						}
						break;
					case 3:// left down
						for (int j = 1; j != directions[i] + 1; j++) {
							newBoard[move.y - j][move.x - j] = currentState.mCurrentPlayer;
						}
						break;
					case 4:// left
						for (int j = 1; j != directions[i] + 1; j++) {
							newBoard[move.y][move.x - j] = currentState.mCurrentPlayer;
						}
						break;
					case 5:// left up
						for (int j = 1; j != directions[i] + 1; j++) {
							newBoard[move.y + j][move.x - j] = currentState.mCurrentPlayer;
						}
						break;
					case 6:// up
						for (int j = 1; j != directions[i] + 1; j++) {
							newBoard[move.y + j][move.x] = currentState.mCurrentPlayer;
						}
						break;
					case 7:// right up
						for (int j = 1; j != directions[i] + 1; j++) {
							newBoard[move.y + j][move.x + j] = currentState.mCurrentPlayer;
						}
						break;
					}
				}
			}
			// after initialized new board
			// package them into a new move
			int newColor = 0;
			if (currentState.mCurrentPlayer == 1)
				newColor = 2;
			else if (currentState.mCurrentPlayer == 2)
				newColor = 1;
			State newState = new State(newColor, newBoard);
			return newState;
		}
	}

	// ==========================================
	// terminal-test
	public boolean Terminal_Test(int depth, final State current) {
		if (depth <= 0)
			return true;
		else {
			State copyOfCurrent = current;
			Vector<Grid> mGrids = new Vector<Grid>();
			for (int i = 0; i != 8; i++) {
				for (int j = 0; j != 8; j++) {
					if (copyOfCurrent.mBoard[i][j] == 0)
						mGrids.add(new Grid(j, i));
				}
			}
			for (int i = 0; i != mGrids.size(); i++) {
				if (actionLegal(copyOfCurrent, mGrids.elementAt(i)))
					return false;
			}
			if (copyOfCurrent.mCurrentPlayer == 1)
				copyOfCurrent.mCurrentPlayer = 2;
			else
				copyOfCurrent.mCurrentPlayer = 1;
			for (int i = 0; i != mGrids.size(); i++) {
				if (actionLegal(copyOfCurrent, mGrids.elementAt(i)))
					return false;
			}
			return true;
		}
	}

	// ==========================================
	public double Max(double a, double b) {
		if (a >= b)
			return a;
		else
			return b;
	}

	public double Min(double a, double b) {
		if (a <= b)
			return a;
		else
			return b;
	}

	// ==========================================
	// max-value
	public double Max_Value(int mobility, int depth, final State current) {
		if (Terminal_Test(depth, current))
			return Utility(mobility, current);
		else {
			double value = -Double.MAX_VALUE;
			Vector<Grid> mActions = Actions(current);
			mobility += mActions.size();
			for (int i = 0; i != mActions.size(); i++) {
				value = Max(
						value,
						Min_Value(mobility, depth - 1,
								Result(current, mActions.elementAt(i))));
			}
			return value;
		}
	}

	// ==========================================
	// min-value
	public double Min_Value(int mobility, int depth, final State current) {
		if (Terminal_Test(depth, current))
			return Utility(mobility, current);
		else {
			double value = Double.MAX_VALUE;
			Vector<Grid> mActions = Actions(current);
			mobility -= mActions.size();
			for (int i = 0; i != mActions.size(); i++) {
				value = Min(
						value,
						Max_Value(mobility, depth - 1,
								Result(current, mActions.elementAt(i))));
			}
			return value;
		}
	}

	// ==========================================
	// actions
	public Vector<Grid> Actions(final State current) {
		Vector<Grid> mGrids = new Vector<Grid>();
		for (int i = 0; i != 8; i++) {
			for (int j = 0; j != 8; j++) {
				if (current.mBoard[i][j] == 0)
					mGrids.add(new Grid(j, i));
			}
		}
		Vector<Grid> mActions = new Vector<Grid>();
		for (int i = 0; i != mGrids.size(); i++) {
			if (actionLegal(current, mGrids.elementAt(i)))
				mActions.add(mGrids.elementAt(i));
		}
		return mActions;
	}

	// ==========================================
	// mini-max-decision function
	public Grid MiniMax_Decision(final State current) {
		Vector<Grid> mActions = Actions(current);
		if (mActions.isEmpty()) {
			return null;
		} else {
			int depth = 4;// end in Max_Value
			// it is very important to make sure
			// the mini max search end in Max_Value
			// because the strategy include making traps

			// end it is very important to end in Max,
			// because of calculating mobilities
			int mobility = mActions.size();
			Grid action = null;
			double highestScore = -Double.MAX_VALUE;
			for (int n = 0; n != mActions.size(); n++) {
				double tempScore = Min_Value(mobility, depth - 1,
						Result(current, mActions.elementAt(n)));
				if (highestScore < tempScore) {
					highestScore = tempScore;
					action = mActions.elementAt(n);
				}
			}
			return action;
		}
	}

	public void behave() {
		this.turnCount++;
		State current = new State(mColor, board);
		Grid move = MiniMax_Decision(current);
		if (move != null) {// move exist
			mSend.message = new String("M" + move.x + move.y);
			System.out.println(mSend.message);
			printBoard(Result(current, move));
			out.println(mSend.message);
			out.flush();
		} else {
			System.out.println("there is no legal moves.");
		}
	}

	public void updateBoard(String receive) {
		// update board array
		for (int i = 1; i != receive.length(); i++) {
			int n = i - 1;
			int x = n / 8;
			int y = n % 8;
			board[y][x] = receive.charAt(i) - '0';
		}
	}

	public void printBoard(final State aState) {
		// System.out.println("\nChess Borad :");
		System.out.print('\n');
		// i - horizontal row, j - vertical row
		for (int i = 7; i != -1; i--) {// each row
										// print marks
			for (int j = 0; j != 8; j++) {
				if (j == 0) {
					System.out.print(i);
					System.out.print(' ');
				}
				System.out.print(' ');
				if (aState.mBoard[i][j] == 0)
					System.out.print(' ');// empty
				else if (aState.mBoard[i][j] == 1)
					System.out.print('O');// white
				else if (aState.mBoard[i][j] == 2)
					System.out.print('X');// black
				else
					System.out.print('!');
				System.out.print(' ');
				if (j != 7)
					System.out.print('|');
			}
			System.out.print('\n');
			// print numbers
			if (i == 0) {// last row
				for (int j = 0; j != 8; j++) {// each column
					if (j == 0)
						System.out.print("  ");
					// print the grid number
					System.out.print(' ');
					System.out.print(j);
					System.out.print(' ');
					if (j != 7)
						System.out.print(' ');
				}
				System.out.print('\n');
			}
			// print grids
			else {// other rows
				for (int j = 0; j != 8; j++) {// each column
					if (j == 0)
						System.out.print("  ");
					System.out.print("---");
					if (j != 7)
						System.out.print('+');
				}
				System.out.print('\n');
			}
		}
	}

	// print a friendly readable board with the received message string
	public void printMyBoard(String receive) {
		// print play side
		System.out.print("Your color is ");
		if (mColor == 2)
			System.out.print("Black");
		else
			System.out.print("White");
		System.out.print("       ");
		System.out.print("Your piece is ");
		if (mColor == 2)
			System.out.print('X');// black
		else
			System.out.print('O');// white
		System.out.print('\n');
		State current = new State(this.mColor, this.board);
		printBoard(current);
		// System.out.println("Type Mxy : x is column number, y is row number.");
	}
}

